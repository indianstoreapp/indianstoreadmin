package com.example.indianstoreadmin;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.indianstoreadmin.activity.DashboardActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity {
    private Button login_btn;
    private TextView  forgot_pass_tv;
    private TextInputEditText password_et, email_et;
     FirebaseAuth fAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        login_btn = findViewById(R.id.login_btn);
        forgot_pass_tv = findViewById(R.id.forget_password_tv);
        password_et = findViewById(R.id.password_et);
        email_et = findViewById(R.id.email_et);
        fAuth=FirebaseAuth.getInstance();
        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
    }
    public void login() {
        if (email_et.getText().toString().length() == 0) {
            email_et.setError(getString(R.string.please_enter_email));
            email_et.requestFocus();
            return;
        }
        if (password_et.getText().toString().length() == 0) {
            password_et.setError(getString(R.string.please_enter_password));
            password_et.requestFocus();
            return;
        }
        String email=email_et.getText().toString().trim();
        String password=password_et.getText().toString().trim();
        fAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
//                    Toast.makeText(LoginActivity.this,"Logged in Successfull",Toast.LENGTH_SHORT).show();
                    Intent intent= new Intent(LoginActivity.this, DashboardActivity.class);
                    startActivity(intent);
                    LoginActivity.this.finish();
                }else{
                    Toast.makeText(LoginActivity.this,"Error ! "+task.getException().getMessage(),Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}