package com.example.indianstoreadmin.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.FrameLayout;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.example.indianstoreadmin.fragment.DashboardFragment;
import com.example.indianstoreadmin.R;
import com.example.indianstoreadmin.fragment.CategoryFragment;
import com.google.android.material.navigation.NavigationView;

public class DashboardActivity extends AppCompatActivity {
    private DrawerLayout drawerLayout;
    private CoordinatorLayout coordinatorLayout;
    private Toolbar toolbar;
    private FrameLayout frameLayout;
    private NavigationView navigationView;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        drawerLayout=findViewById(R.id.drawerLaybout);
        coordinatorLayout=findViewById(R.id.coordinatorLayout);
        toolbar=findViewById(R.id.toolbar);
        frameLayout=findViewById(R.id.frameLayout);
        navigationView=findViewById(R.id.navigationView);
        setUpToolbar();
        openDasboard();
        actionBarDrawerToggle = new ActionBarDrawerToggle(DashboardActivity.this,drawerLayout,R.string.open_drawer,R.string.close_drawer);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
               switch (item.getItemId()){
                   case R.id.dashboard:
                      getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout,new DashboardFragment()).commit();
                       getSupportActionBar().setTitle("Dashboard");
                      drawerLayout.closeDrawers();
                       return true;
                   case R.id.category:
//                       Toast.makeText(getApplicationContext(),"Category",Toast.LENGTH_SHORT).show();
                       getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout,new CategoryFragment()).commit();
                       getSupportActionBar().setTitle("Category");
                       drawerLayout.closeDrawers();
                       return true;
               }
                return true;
            }
        });
    }
    public void setUpToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Indian Store");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id=item.getItemId();
        if(id==android.R.id.home){
            drawerLayout.openDrawer(GravityCompat.START);
        }
        return super.onOptionsItemSelected(item);
    }
    public void openDasboard(){
        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout,new DashboardFragment()).commit();
        getSupportActionBar().setTitle("Dashboard");
        navigationView.setCheckedItem(R.id.dashboard);
    }

    @Override
    public void onBackPressed() {
        Fragment frag;
        frag= getSupportFragmentManager().findFragmentById(R.id.frameLayout);

        super.onBackPressed();
    }
}