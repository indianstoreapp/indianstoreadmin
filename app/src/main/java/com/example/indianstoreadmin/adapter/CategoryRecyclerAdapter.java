package com.example.indianstoreadmin.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.indianstoreadmin.R;

import java.util.ArrayList;

public class CategoryRecyclerAdapter extends RecyclerView.Adapter<CategoryRecyclerAdapter.CategoryViewHolder> {

    private Context context;
    ArrayList<String> itemlist;

    public CategoryRecyclerAdapter(Context context, ArrayList<String> cat) {
        this.context=context;
        this.itemlist=cat;
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull  ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_category_single_row, parent, false);
        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryRecyclerAdapter.CategoryViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return itemlist.size();
    }

    class CategoryViewHolder extends RecyclerView.ViewHolder{
        TextView textView;
        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            textView=itemView.findViewById(R.id.txtCategoryName);
        }
    }
}
