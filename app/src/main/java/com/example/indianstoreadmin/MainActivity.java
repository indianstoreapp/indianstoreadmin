package com.example.indianstoreadmin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.indianstoreadmin.activity.DashboardActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new Handler().postDelayed(
                () -> {
                    Intent intent= new Intent(MainActivity.this, DashboardActivity.class);
                    startActivity(intent);
                    MainActivity.this.finish();
                }
                , 1500);
    }
}